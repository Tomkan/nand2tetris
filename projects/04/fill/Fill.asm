// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel. When no key is pressed, the
// program clears the screen, i.e. writes "white" in every pixel.

(LOOP1)
@SCREEN
D=A
@ind
M=D
@pix
M=0
@KBD
D=M
@LOOP2
D;JEQ
@pix
M=-1
(LOOP2)
@ind
D=M
@KBD
D=D-A
@LOOP1
D;JEQ
@pix
D=M
@ind
A=M
M=D
@ind
M=M+1
@LOOP2
0;JMP
